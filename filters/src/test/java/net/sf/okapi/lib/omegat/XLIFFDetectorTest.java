package net.sf.okapi.lib.omegat;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

public class XLIFFDetectorTest {
	
	@Test
	public void testXLIFF12 ()
		throws URISyntaxException, XMLStreamException, IOException
	{
		File file = new File(getClass().getResource("/example_v12.xlf").toURI());
		assertEquals("1.2", XLIFFDetector.getXLIFFVersion(file));
	}

	@Test
	public void testXLIFF20 ()
		throws URISyntaxException, XMLStreamException, IOException
	{
		File file = new File(getClass().getResource("/example_v20.xlf").toURI());
		assertEquals("2.0", XLIFFDetector.getXLIFFVersion(file));
	}

	@Test (expected = IOException.class)
	public void testNoVersion ()
		throws URISyntaxException, XMLStreamException, IOException
	{
		File file = new File(getClass().getResource("/no-version.xlf").toURI());
		XLIFFDetector.getXLIFFVersion(file);
	}

	@Test (expected = IOException.class)
	public void testNotXLIFF ()
		throws URISyntaxException, XMLStreamException, IOException
	{
		File file = new File(getClass().getResource("/not-xliff.xlf").toURI());
		XLIFFDetector.getXLIFFVersion(file);
	}

}
