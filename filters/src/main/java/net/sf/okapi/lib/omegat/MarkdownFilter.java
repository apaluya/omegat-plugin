package net.sf.okapi.lib.omegat;

import org.omegat.filters2.Instance;

public class MarkdownFilter extends AbstractOkapiFilter {

    private static final String EXTENSION = ".md";
    
    public MarkdownFilter () {
        initialize("Markdown files (Okapi)",
            "net.sf.okapi.filters.markdown.MarkdownFilter",
            "okf_markdown",
            EXTENSION);
    }

    @Override
    public Instance[] getDefaultInstances () {
        return new Instance[] {
            new Instance("*"+EXTENSION)
        };
    }

}
